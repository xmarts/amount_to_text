from odoo import models, fields, api
import amount_to_text_es

class AmmountToTextPurchaseOrder(models.Model):
    _name = 'purchase.order'
    _inherit = 'purchase.order'

    amount_to_text = fields.Char(compute='_get_amount_to_text', string='Amount to Text', store=True,
                                help='Amount of the invoice in letter')

    @api.one
    @api.depends('amount_total', 'currency_id')
    def _get_amount_to_text(self):
        self.amount_to_text = amount_to_text_es.get_amount_to_text(self, self.amount_total, self.currency_id.name)

