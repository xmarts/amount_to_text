{
    'name': 'Amount to text',
    'version': '10.0.0.1',
    'summary': 'Amount to text in sale order and purchase order',
    'description': 'In sale order and purchase order add a field that is called "amount_to_text" which puts the total amounts in letters',
    'author': 'Raul Ovalle, raul@xmarts.do',
    'website': 'www.xmarts.net',
    'depends': ['sale','purchase'],
    'data': [],
    'installable': True,
}